package com.codelab.marklogic.example;

import com.marklogic.client.document.DocumentDescriptor;
import com.marklogic.client.document.TextDocumentManager;
import com.marklogic.client.io.StringHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OptimisticLockingExamples {

    @Autowired
    private TextDocumentManager textDocumentManager;

    public void write(String content, String docId) {
        DocumentDescriptor documentDescriptor = textDocumentManager.newDescriptor(docId);
        StringHandle handle = new StringHandle(content);
        textDocumentManager.write(documentDescriptor, handle);
    }

}
