package com.codelab.marklogic.example;

import com.marklogic.client.io.SearchHandle;
import com.marklogic.client.query.QueryManager;
import com.marklogic.client.query.StructuredQueryBuilder;
import com.marklogic.client.query.StructuredQueryDefinition;
import com.marklogic.client.query.SuggestDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StructuredQueryExamples {

    @Autowired
    private QueryManager queryManager;

    public SearchHandle searchByTermAndValue(String persistentQueryOption, String term, String industry, String date, String title) {
        StructuredQueryBuilder queryBuilder = new StructuredQueryBuilder(persistentQueryOption);

        StructuredQueryDefinition termQuery = queryBuilder.term(term);
        StructuredQueryDefinition valueConstraint = queryBuilder.valueConstraint("industry", industry);

        StructuredQueryDefinition queryDefinition = queryBuilder.and(termQuery, valueConstraint);

        return queryManager.search(queryDefinition, new SearchHandle());
    }

    public SearchHandle searchByRangeConstraint(String date) {
        StructuredQueryBuilder queryBuilder = new StructuredQueryBuilder();
        StructuredQueryDefinition queryDefinition = queryBuilder.rangeConstraint("date", StructuredQueryBuilder.Operator.EQ, date);
        return queryManager.search(queryDefinition, new SearchHandle());
    }

    public SearchHandle searchByWordConstraint(String title) {
        StructuredQueryBuilder queryBuilder = new StructuredQueryBuilder();
        StructuredQueryDefinition queryDefinition = queryBuilder.wordConstraint("title", title);
        return queryManager.search(queryDefinition, new SearchHandle());
    }

    public SearchHandle searchByProperty(String propertyTerm) {
        StructuredQueryBuilder queryBuilder = new StructuredQueryBuilder();
        StructuredQueryDefinition termQuery = queryBuilder.term(propertyTerm);
        StructuredQueryDefinition queryDefinition = queryBuilder.properties(termQuery);
        return queryManager.search(queryDefinition, new SearchHandle());
    }

    public SearchHandle searchByDirectory(String... directories) {
        StructuredQueryBuilder queryBuilder = new StructuredQueryBuilder();
        StructuredQueryDefinition queryDefinition = queryBuilder.directory(true, directories);
        return queryManager.search(queryDefinition, new SearchHandle());
    }

    public SearchHandle searchByCollection(String... collections) {
        StructuredQueryBuilder queryBuilder = new StructuredQueryBuilder();
        StructuredQueryDefinition queryDefinition = queryBuilder.collection(collections);
        return queryManager.search(queryDefinition, new SearchHandle());
    }

    public String[] suggest(String criteria, int pageSize) {
        SuggestDefinition suggestDefinition = queryManager.newSuggestDefinition();
        suggestDefinition.setStringCriteria(criteria);
        suggestDefinition.setLimit(pageSize);
        return queryManager.suggest(suggestDefinition);
    }

}
