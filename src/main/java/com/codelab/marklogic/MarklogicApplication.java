package com.codelab.marklogic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarklogicApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarklogicApplication.class, args);
	}

}
