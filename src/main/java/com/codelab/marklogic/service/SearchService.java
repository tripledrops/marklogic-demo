package com.codelab.marklogic.service;

import com.marklogic.client.io.SearchHandle;
import com.marklogic.client.query.QueryManager;
import com.marklogic.client.query.StringQueryDefinition;
import com.marklogic.client.query.StructuredQueryBuilder;
import com.marklogic.client.query.StructuredQueryDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SearchService {

    @Autowired
    private QueryManager queryManager;

    public SearchHandle searchByString(String queryCriteria) {
        StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
        queryDefinition.setCriteria(queryCriteria);
        return queryManager.search(queryDefinition, new SearchHandle());
    }

    public SearchHandle searchByStructuredQuery(StructuredQueryDefinition queryDefinition) {
        return queryManager.search(queryDefinition, new SearchHandle());
    }

}
