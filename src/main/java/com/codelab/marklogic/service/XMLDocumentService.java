package com.codelab.marklogic.service;

import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.FileHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import java.io.File;

@Component
public class XMLDocumentService {

    @Autowired
    private XMLDocumentManager xmlDocumentManager;

    public void update(File file, String docId) {
        FileHandle handle = new FileHandle(file);
        xmlDocumentManager.write(docId, handle);
    }

    public Document read(String docId) {
        DOMHandle handle = new DOMHandle();
        xmlDocumentManager.read(docId, handle);
        return handle.get();
    }

}
