package com.codelab.marklogic.service;

import com.marklogic.client.document.DocumentDescriptor;
import com.marklogic.client.document.DocumentUriTemplate;
import com.marklogic.client.document.TextDocumentManager;
import com.marklogic.client.io.StringHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TextDocumentService {

    @Autowired
    private TextDocumentManager textDocumentManager;

    public void write(String content, String docId) {
        StringHandle handle = new StringHandle();
        handle.set(content);
        textDocumentManager.write(docId, handle);
    }

    public DocumentDescriptor create(String content, String directory, String extension) {
        DocumentUriTemplate uriTemplate = textDocumentManager.newDocumentUriTemplate(extension);
        uriTemplate.setDirectory(directory);

        StringHandle handle = new StringHandle();
        handle.set(content);

        return textDocumentManager.create(uriTemplate, handle);
    }

    public void delete(String docId) {
        textDocumentManager.delete(docId);
    }

}
