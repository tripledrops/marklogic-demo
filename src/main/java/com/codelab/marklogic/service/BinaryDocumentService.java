package com.codelab.marklogic.service;

import com.marklogic.client.document.BinaryDocumentManager;
import com.marklogic.client.io.BytesHandle;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.FileHandle;
import com.marklogic.client.io.InputStreamHandle;
import com.marklogic.client.io.marker.DocumentMetadataReadHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.InputStream;

@Component
public class BinaryDocumentService {

    @Autowired
    private BinaryDocumentManager binaryDocumentManager;

    public void write(File file, String mimeType, String docId) {
        FileHandle handle = new FileHandle().with(file).withMimetype(mimeType);
        binaryDocumentManager.write(docId, handle);
    }

    public InputStream readAsStream(String docId) {
        DocumentMetadataHandle metadataHandle = new DocumentMetadataHandle();
        return binaryDocumentManager.read(docId, metadataHandle, new InputStreamHandle()).get();
    }

    public byte[] readAsByteArray(String docId) {
        DocumentMetadataHandle metadataHandle = new DocumentMetadataHandle();
        return binaryDocumentManager.read(docId, metadataHandle, new BytesHandle()).get();
    }

    public DocumentMetadataReadHandle readMetadata(String docId) {
        DocumentMetadataHandle handle = new DocumentMetadataHandle();
        return binaryDocumentManager.readMetadata(docId, handle);
    }

}
