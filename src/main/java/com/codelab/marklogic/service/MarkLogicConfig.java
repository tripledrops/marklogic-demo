package com.codelab.marklogic.service;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.admin.ServerConfigurationManager;
import com.marklogic.client.document.BinaryDocumentManager;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.document.TextDocumentManager;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.query.QueryManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MarkLogicConfig {

    @Value("${marklogic.host}")
    private String host;

    @Value("${marklogic.port}")
    private int port;

    @Value("${marklogic.username}")
    private String username;

    @Value("${marklogic.password}")
    private String password;

    @Bean
    public DatabaseClient databaseClient() {
        DatabaseClient client = DatabaseClientFactory.newClient(
                host, port,
                new DatabaseClientFactory.DigestAuthContext(username, password));

        // Enable optional optimistic locking
        ServerConfigurationManager configurationManager = client.newServerConfigManager();
        configurationManager.readConfiguration();
        configurationManager.setUpdatePolicy(ServerConfigurationManager.UpdatePolicy.VERSION_OPTIONAL);
        configurationManager.writeConfiguration();

        return client;
    }

    @Bean
    public XMLDocumentManager xmlDocumentManager() {
        return databaseClient().newXMLDocumentManager();
    }

    @Bean
    public JSONDocumentManager jsonDocumentManager() {
        return databaseClient().newJSONDocumentManager();
    }

    @Bean
    public TextDocumentManager textDocumentManager() {
        return databaseClient().newTextDocumentManager();
    }

    @Bean
    public BinaryDocumentManager binaryDocumentManager() {
        BinaryDocumentManager binaryDocumentManager = databaseClient().newBinaryDocumentManager();
        binaryDocumentManager.setMetadataExtraction(BinaryDocumentManager.MetadataExtraction.PROPERTIES);
        return binaryDocumentManager;
    }

    @Bean
    public QueryManager queryManager() {
        return databaseClient().newQueryManager();
    }


}
