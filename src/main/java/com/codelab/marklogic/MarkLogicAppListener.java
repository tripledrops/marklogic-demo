package com.codelab.marklogic;


import com.marklogic.client.DatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.stereotype.Component;

@Component
public class MarkLogicAppListener implements ApplicationListener<ContextStoppedEvent> {

    @Autowired
    private DatabaseClient databaseClient;

    @Override
    public void onApplicationEvent(ContextStoppedEvent contextStoppedEvent) {
        this.databaseClient.release();
    }


}
