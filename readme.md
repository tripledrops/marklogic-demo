# MarkLogic Sample Codes

`com.codelab.marklogic.service.MarkLogicConfig` class

* Creates singleton `DatabaseClient`, `XMLDocumentManager`, `JSONDocumentManager`, `TextDocumentManager`, `BinaryDocumentManager`, `QueryManager` objects, as these objects are thread safe. 

`com.codelab.marklogic.service.TextDocumentService` class

* Shows how to create, update and delete a text document

`com.codelab.marklogic.service.XMLDocumentService` class

* Shows how to read and update an XML document

`com.codelab.marklogic.service.BinaryDocumentService` class

* Shows how to write binary document, read binary document back as `InputStream` and byte array, and read document metadata.

`com.codelab.marklogic.example.StructuredQueryExamples`

* Shows various ways to compose structured queries

`com.codelab.marklogic.example.OptimisticLockingExamples` class

* Shows how to use optimistic locking for concurrency

`com.codelab.marklogic.MarkLogicAppListener`

* Releases singleton `DatabaseClient` object when Spring container shuts down.
